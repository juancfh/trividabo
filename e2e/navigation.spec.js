describe('Navigation between scenes', () => {
    beforeAll(async () => {
        await page.goto('http://localhost:3000');
    });
  
    it('should be titled "Trividabo"', async () => {
        await expect(page.title()).resolves.toMatch('Trividabo');
    });

    it('goes to /board on click start game"', async () => {
        await page.click('.welcome__button');
        await expect(page.url()).toMatch('/board');
    });

    it('goes to /welcome on click play again button"', async () => {
        await page.click('.board__button');
        await expect(page.url()).toMatch('/welcome');
    });
  });