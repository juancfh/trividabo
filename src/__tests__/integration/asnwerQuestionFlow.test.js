jest.mock("react-router-dom");

import React  from 'react';
import {render, fireEvent, cleanup, wait} from 'react-testing-library'
import Board from '../../scenes/board/board';
import Question from '../../scenes/board/components/questionList/components/question/question';
import QuestionList from '../../scenes/board/components/questionList/questionList';
import QuestionFormAnswers from '../../scenes/board/components/questionList/components/question/components/questionFormAnswers/questionFormAnswers';
import mockQuestionsParsed from '../helpers/test-api-data-parsed.json';

let mockHandleAnwerQuestionFormAnswer;
let mockHandleQuestion;
let mockHandleQuestionList;
let mockHandleBoard;

beforeAll(() => {
    let mockQuestion0 = mockQuestionsParsed[0];
    let mockQuestion1 = mockQuestionsParsed[1];
    mockQuestion0.isToggle = false;
    mockQuestion1.isToggle = false;
    jest.spyOn(Board.prototype, 'fetchQuestion').mockResolvedValueOnce(mockQuestion0).mockResolvedValueOnce(mockQuestion1);
    mockHandleAnwerQuestionFormAnswer = jest.spyOn(QuestionFormAnswers.prototype, 'handleAnswerQuestion');
    mockHandleQuestion= jest.spyOn(Question.prototype, 'handleAnswerQuestion');
    mockHandleQuestionList= jest.spyOn(QuestionList.prototype, 'handleAnswerQuestion');
    mockHandleBoard= jest.spyOn(Board.prototype, 'handleAnswerQuestion');
})

afterEach(() => {
    cleanup();
    jest.clearAllMocks();
});

describe('Answer question flow', () => {
    //ToDo: Could be good idea use ToBeCalledWith to ensure it propagates carrect payloads
    it('propagates click envent on confim button', async() => {
        const {getByText} = await render(<Board/>);
        await wait(() => {fireEvent.click(getByText('Confirm'))});
        expect(mockHandleAnwerQuestionFormAnswer).toBeCalledTimes(1);
        expect(mockHandleQuestion).toBeCalledTimes(1);
        expect(mockHandleQuestionList).toBeCalledTimes(1);
        expect(mockHandleBoard).toBeCalledTimes(1);
    });

    //ToDo:Technical deb
    xit('updates progress bar and counter state', async() => {
    });

});
  