jest.mock("react-router-dom");
jest.mock("axios");

import React, { Component }  from 'react';
import ReactDOM from 'react-dom';
import Board from '../../../../scenes/board/board';
import mockQuestions from '../../../helpers/test-api-data.json';
import mockQuestionsParsed from '../../../helpers/test-api-data-parsed.json';
import * as axios from 'axios';
import each from 'jest-each';

let boardClass;
let spyFetchQuestion;
let spyParseQuestion;
let spyGenerateAnswers;
let spyOnSetState;

beforeAll(() => {
    spyOnSetState = jest.spyOn(Component.prototype, 'setState').mockReturnValue();
    boardClass = new Board();
    spyFetchQuestion = jest.spyOn(boardClass, 'fetchQuestion');
    spyParseQuestion = jest.spyOn(boardClass, 'buildQuestion');
    spyGenerateAnswers = jest.spyOn(boardClass, 'generateAnswers');
})

afterEach(() => {
    jest.clearAllMocks();
});

describe('Board component unit', () => {
    
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Board />, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    describe('fetchQuestion', () => {
        it('fetch and build question propertly', async () => {
            axios.get.mockResolvedValue({data: mockQuestions[0]});
            spyGenerateAnswers.mockReturnValue([5,6,7,8]);
            boardClass.fakeState = {questions: []};
            let question = await boardClass.fetchQuestion();
            expect(question).toEqual(mockQuestionsParsed[0]);
            spyGenerateAnswers.mockRestore();
        });
        it('returns null when can not fetch question', async () => {
            axios.get.mockRejectedValue();
            let question = await boardClass.fetchQuestion();
            expect(question).toEqual(null);
        });

    });

    describe('generateAnswers', () => {
        each([[1], [8], [10]])
        .it(
            'generate answers with %d as correct aswer',
            async (number) => {
                let answers = boardClass.generateAnswers(number);
                expect(answers).toHaveLength(4);
                expect(answers.indexOf(number)).not.toEqual(-1); //ToDo: Search for native form in JEST
            },
        );

    });

    describe('generateRandomInteger', () => {
        each`
            min   | max 
            ${0}  | ${1}
            ${0}  | ${100}
            ${23} | ${24} 
            `.it('generate random answers between $min and $max', ({min, max}) => {
                let integer = boardClass.generateRandomInteger(min, max);
                expect(integer).toBeGreaterThanOrEqual(min)
                expect(integer).toBeLessThanOrEqual(max);
            });
    });

    describe('handleAnswerQuestion', () => {
        it('update state correctly', async() => {
            let fakeState = {questions: [...mockQuestionsParsed]};
            boardClass.state = fakeState;
            let fakeQuestion = {
                description: "What is the traditional age for retirement in the United Kingdom, Germany and other countries?",
                number: "65",
                isToggle: true,
                isAnswered: true,
                answer: 65,
                answers: [5,6,7,8]
            }
            await boardClass.handleAnswerQuestion(fakeQuestion);
            fakeState.questions[0] = fakeQuestion;
            expect(spyOnSetState).toBeCalledWith(fakeState);
        });
        it('only fetchs 10 questions', async() => {
            let fakeState = {questions: [...mockQuestionsParsed]};
            boardClass.state = fakeState;
            await boardClass.handleAnswerQuestion(mockQuestionsParsed[0]);
            expect(spyFetchQuestion).not.toBeCalled();
        });
        it('fetchs new question', async() => {
            let newQuestion = {...mockQuestionsParsed[0]};
            newQuestion.number = '9999';
            mockQuestionsParsed.splice(0,1);
            axios.get.mockResolvedValue({data: newQuestion});
            let fakeState = {questions: [...mockQuestionsParsed]};
            boardClass.state = fakeState;
            await boardClass.handleAnswerQuestion(mockQuestionsParsed[1]);
            expect(spyFetchQuestion).toBeCalledTimes(1);
        });
        it('tries 5 times if API does not response', async() => {
            axios.get.mockRejectedValue(null);
            mockQuestionsParsed.splice(0,1);
            let fakeState = {questions: [...mockQuestionsParsed]};
            boardClass.state = fakeState;
            await boardClass.handleAnswerQuestion(mockQuestionsParsed[0]);
            expect(spyFetchQuestion).toBeCalledTimes(5);
        });
        it('tries fetch question until get one or 3 times', async() => {
            axios.get.mockRejectedValueOnce(null);
            axios.get.mockResolvedValue({data: mockQuestions[0]});
            mockQuestionsParsed.splice(0,1);
            let fakeState = {questions: [...mockQuestionsParsed]};
            boardClass.state = fakeState;
            await boardClass.handleAnswerQuestion(mockQuestionsParsed[0]);
            expect(spyFetchQuestion).toBeCalledTimes(2);
        });
        it('avoids insert nulls on question state array', async() => {
            axios.get.mockRejectedValue(null);
            mockQuestionsParsed.splice(0,3);
            let fakeState = {questions: [...mockQuestionsParsed]};
            boardClass.state = fakeState;
            await boardClass.handleAnswerQuestion(mockQuestionsParsed[0]);
            expect(spyOnSetState).toBeCalledWith(fakeState);
        });
        it('avoids insert duplicated numbers using reties system', async() => {
            axios.get.mockResolvedValue({data: mockQuestionsParsed[0]});
            let fakeState = {questions: [mockQuestionsParsed[0]]};
            boardClass.state = fakeState;
            await boardClass.handleAnswerQuestion(mockQuestionsParsed[0]);
            expect(spyFetchQuestion).toBeCalledTimes(5);
        });
    });

    describe('componentDidMount', () => {
        it('tries to load 1 questions on mount', async () => {
            axios.get.mockResolvedValue({data: mockQuestions[0]});
            await boardClass.componentDidMount();
            expect(spyFetchQuestion).toBeCalledTimes(1);
            expect(spyParseQuestion).toBeCalledTimes(1);
        });
    });

    describe('searchQuestionByNumber', () => {
        it('returns correct question', () => {
            let fakeState = {questions: [...mockQuestionsParsed]};
            boardClass.state = fakeState;
            let question = boardClass.searchQuestionByNumber(mockQuestionsParsed[0].number);
            expect(question).toEqual(mockQuestionsParsed[0]);
        });
        it('returns undefined if it can not find question', () => {
            let fakeState = {questions: []};
            boardClass.state = fakeState;
            let question = boardClass.searchQuestionByNumber(mockQuestionsParsed[0].number);
            expect(question).toEqual(undefined);
        });
    });
});
  