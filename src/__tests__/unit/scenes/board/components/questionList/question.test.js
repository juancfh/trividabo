import React  from 'react';
import ReactDOM from 'react-dom';
import Question from '../../../../../../scenes/board/components/questionList/components/question/question';
import mockQuestionsParsed from '../../../../../helpers/test-api-data-parsed.json';

let questionClass;

beforeAll(() => {
    questionClass = new Question();
})

afterEach(() => {
    jest.clearAllMocks();
});

describe('Question component unit', () => {
    
    it('renders without crashing', () => {
        const div = document.createElement('div');
        const question = mockQuestionsParsed[0];
        ReactDOM.render(<Question question={question}/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });
    
    describe('isCorrectAnswer', () => {
        it('returns true if answer is right', () => {
            let fakeProps = {question: mockQuestionsParsed[0]};
            fakeProps.question.answer = fakeProps.question.number;
            questionClass.props = fakeProps;
            let isCorrect = questionClass.isCorrectAnswer();
            expect(isCorrect).toBeTruthy();
        });
        it('returns false if answer is wrong', () => {
            let fakeProps = {question: mockQuestionsParsed[0]};
            fakeProps.question.answer = null;
            questionClass.props = fakeProps;
            let isCorrect = questionClass.isCorrectAnswer();
            expect(isCorrect).toBeFalsy();
        });
    });

    describe('handleAnswerQuestion', () => {
        it('updates currect question before propagate it', () => {
            let fakeQuestion = {question: mockQuestionsParsed[0]};
            let fakeProps = {question: fakeQuestion, onAnswer: jest.fn()};
            let fakeOnAnswerEvent = jest.spyOn(fakeProps, 'onAnswer');
            questionClass.props = fakeProps;
            questionClass.handleAnswerQuestion("999");
            fakeQuestion.answer = "999";
            fakeQuestion.isAnswered = true;
            fakeQuestion.isToggle = true;
            expect(fakeOnAnswerEvent).toBeCalledWith(fakeQuestion);
        });
    });


});
  