import React  from 'react';
import ReactDOM from 'react-dom';
import QuestionFormAnswers from '../../../../../../../scenes/board/components/questionList/components/question/components/questionFormAnswers/questionFormAnswers';

let questionFormAnswersClass;
let spyNewAnswer;
let answers;

beforeAll(() => {
    questionFormAnswersClass = new QuestionFormAnswers();
    spyNewAnswer = jest.spyOn(questionFormAnswersClass, 'newAnswer');
    answers = [5,6,7,8];
})

afterEach(() => {
    jest.clearAllMocks();
});

describe('questionFormAnswers component unit', () => {
    
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<QuestionFormAnswers isToggle={true} answers={answers}/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    describe('questionListComponents', () => {
        it('returns a list of Question component', () => {
            let answerComponents = questionFormAnswersClass.answerListComponents(answers);
            expect(answerComponents).toHaveLength(4);
            expect(spyNewAnswer).toBeCalledTimes(4);
        });
    });
    
});
  