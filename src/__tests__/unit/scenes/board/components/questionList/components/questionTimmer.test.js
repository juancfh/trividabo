import React  from 'react';
import ReactDOM from 'react-dom';
import QuestionTimmer from '../../../../../../../scenes/board/components/questionList/components/question/components/questionTimmer/questionTimmer';

beforeAll(() => {
})

afterEach(() => {
    jest.clearAllMocks();
});

describe('QuestionTimmer component unit', () => {
    
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<QuestionTimmer isTogger={false} time={30} onTime={jest.fn()}/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    //ToDo: Snapshots
    // xdescribe('isToggle', () => {
    //     xit('returns null', () => {
    //     });
    //     xit('returns component', () => {
    //     });
    // });
    
});
  