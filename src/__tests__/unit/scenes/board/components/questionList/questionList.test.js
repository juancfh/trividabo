import React  from 'react';
import ReactDOM from 'react-dom';
import QuestionList from '../../../../../../scenes/board/components/questionList/questionList';
import mockQuestionsParsed from '../../../../../helpers/test-api-data-parsed.json';

let questionListClass;
let spyNewQuestion;

beforeAll(() => {
    questionListClass = new QuestionList();
    spyNewQuestion = jest.spyOn(questionListClass, 'newQuestion');
})

afterEach(() => {
    jest.clearAllMocks();
});

describe('QuestionList component unit', () => {
    
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<QuestionList questions={[]}/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    describe('questionListComponents', () => {
        it('returns a list of Question component', () => {
            let questionsComponents = questionListClass.questionListComponents(mockQuestionsParsed);
            expect(questionsComponents).toHaveLength(10);
            expect(spyNewQuestion).toBeCalledTimes(10);
        });
    });
});
  