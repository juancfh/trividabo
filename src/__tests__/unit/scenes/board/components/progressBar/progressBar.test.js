import React  from 'react';
import ReactDOM from 'react-dom';
import ProgressBar from '../../../../../../scenes/board/components/progressBar/progressBar';

describe('ProgressBar component unit', () => {
    
    it('renders without crashing', async () => {
        const div = document.createElement('div');
        ReactDOM.render(<ProgressBar value={5} max={10}/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });

});