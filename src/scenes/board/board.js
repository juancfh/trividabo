import React, { Component } from 'react';
import QuestionList from './components/questionList/questionList';
import ProgressBar from './components/progressBar/progressBar';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import * as axios from 'axios';
import './board.scss';

const numberQuestions = 10;

class Board extends Component {

    constructor(props) {
        super(props);
        this.state = {questions: []};
        this.handleAnswerQuestion = this.handleAnswerQuestion.bind(this);
    }

    async componentDidMount() {
        const question = await this.fetchQuestion();
        this.setState({questions: [question]});
    }

    async handleAnswerQuestion(question){
        //REMEMBER: React state is the source of true. Check if answer is answered before answer it.
        if (!this.questionsAnswered(question)) {
            let newQuestions = this.replaceQuestionByNumber(this.state.questions, question.number, question);
            if (this.state.questions.length < 10) {
                let retriesNumber = 5;
                let newQuestion = null;
                let isInvalidQuestion;
                do {
                    isInvalidQuestion = true;
                    newQuestion = await this.fetchQuestion();
                    isInvalidQuestion = (!newQuestion || this.searchQuestionByNumber(newQuestion.number));
                    retriesNumber--;
                } while (retriesNumber > 0 && isInvalidQuestion);
                if (newQuestion) {
                    newQuestions.unshift(newQuestion);
                }
            }
            this.setState({questions: newQuestions});
        }
    }   

    async fetchQuestion() {
        let response = null;
        try {
            let apiResponse = await axios.get('http://numbersapi.com/random/trivia?json&fragment');
            response = apiResponse.data ? this.buildQuestion(apiResponse.data) : null;
        } catch (error) {
            return null;
        }
        return response;
    }

    searchQuestionByNumber(number) {
        let question = this.state.questions.find((qt) => {
            return qt.number === number;
        });
        return question;
    }

    questionsAnswered(question) {
        let selectedQuestion = this.searchQuestionByNumber(question.number);
        return selectedQuestion ? selectedQuestion.isAnswered : true; //If question is not on state, something bad has happened so stop fetch questions, game over!
    }

    buildQuestion(apiResponse) {
        return {
            description: this.sentenceToQuestion(apiResponse.text),
            number: apiResponse.number.toString(),
            isToggle: false,
            isAnswered: false,
            answer: null,
            answers: this.generateAnswers(apiResponse.number)
        }
    }

    generateAnswers(correctAnswer) {
        const generatesAnswers = 3;
        let answers = [];
        let position = 0;
        let random = 0;

        //ToDo: Random positions too
        for (position = 0; position < generatesAnswers; position++) {
            random = this.generateRandomInteger(0,80000);
            if (answers.indexOf(random) !== -1) {
                position--;
            } else {
                answers[position] = random;
            }
        }

        answers[generatesAnswers] = correctAnswer;
        return answers;
    }

    generateRandomInteger(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    sentenceToQuestion(sentence) {
        return "What is " + sentence + "?"
    }

    replaceQuestionByNumber(array, number, newQuestion) {
        let newArray = [...array];
        let modifiedArray = newArray.map ((question) => {
            if (question.number === number) {
                return newQuestion;
            }
            return question;
        });
        return modifiedArray;
    }

    render() {
        return (
            <div className="board">
                <h2>TRIVIDABO</h2>
                <p>QUESTION {this.state.questions.length} OF 10</p>
                <div className="board__progress"><ProgressBar value={this.state.questions.length} max={numberQuestions}></ProgressBar></div>
                <div className="board__question-list">
                    <QuestionList onAnswer={this.handleAnswerQuestion} questions={this.state.questions}></QuestionList>
                </div>
                <Link to="/welcome">
                    <button className="board__button">Play Again</button>
                </Link>
            </div>
        )
    }
}

export default Board;