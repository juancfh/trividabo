import React, { Component } from 'react';
import "./progressBar.scss";

class ProgressBar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <progress value={this.props.value} max={this.props.max}></progress>
        )
    }
}

export default ProgressBar;