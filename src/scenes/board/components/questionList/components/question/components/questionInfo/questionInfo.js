import React, { Component } from 'react';
import './questionInfo.scss';
import failIcon from '../../assets/icon-trividabo-fail.svg';
import sucessIcon from '../../assets/icon-trividabo-success.svg';

class QuestionInfo extends Component {

    constructor(props) {
        super(props);
    }

    selectIcon() {
        let icon = {src: failIcon, alt: "Fail icon"};

        if (this.props.isCorrect) {
            icon = {src: sucessIcon, alt: "Success icon"};
        }

        return icon;
    }

    isToggle (toggle) {
        let component = null;
        let icon = this.selectIcon();
        if (toggle) {
            component = <div className="question__info">
                            <img src={icon.src} alt={icon.alt}/>
                            {this.props.isCorrect ? (
                                <p className="question__info-text-sucess">{this.props.answer}</p>
                            ) : (
                                <p className="question__info-text-normal"> - Right answer was {this.props.answer} </p>
                            )}
                        </div>
        }
        return component;
    }

    render() {
        return (this.isToggle(this.props.isToggle));
    }
}

export default QuestionInfo;