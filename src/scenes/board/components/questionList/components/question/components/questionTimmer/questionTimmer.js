import React, { Component } from 'react';

class QuestionTimmer extends Component {

    constructor(props) {
        super(props);
        this.state = {time: this.props.time} // :S
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.updateTimmer(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    updateTimmer() {
        let currentTime = this.state.time;
        let nextTime = currentTime -1;
        if (nextTime >= 0) {
            this.setState({
                time: nextTime
            });
        } else {
            this.props.onTime(null);
            clearInterval(this.timerID);
        }
    }

    isToggle (toggle) {
        let component = null;
        if (!toggle) {
            component = <div className="question__timmer">
                            <p>{this.state.time}</p>
                        </div>
        }
        return component;
    }

    render() {
        return (this.isToggle(this.props.isToggle));
    }
}

export default QuestionTimmer;