import React, { Component } from 'react';
import './questionFormAnswers.scss';

class QuestionFormAnswers extends Component {

    constructor(props) {
        super(props);
        this.state = {userAnswer: null}
        this.handleAnswerQuestion = this.handleAnswerQuestion.bind(this);
    }

    handleAnswerQuestion(event) {
        event.preventDefault();
        this.props.onAnswer(this.state.userAnswer);
    }

    handleChangeAnswer = (event) => {
        this.setState({userAnswer: event.target.value});
    }

    newAnswer(answer, index) {
        return <div className="question__answer" key={index}><input onChange={this.handleChangeAnswer} type="radio" value={answer} name="answers"/>{answer}</div>;
    }

    answerListComponents(answers) {
        let listComponents = answers.map((answer, index) => this.newAnswer(answer, index));
        return listComponents;
    }

    isToggle (toggle) {
        let component = null;
        let asnwerList = this.answerListComponents(this.props.answers);
        if (!toggle) {
            component = <form onSubmit={this.handleAnswerQuestion}>
                            <div className="question__answers">{asnwerList}</div>
                            <div className="question__menu">
                                <button type="submit" value="Submit">Confirm</button>
                            </div>
                        </form>
        }
        return component;
    }

    render() {
        return (this.isToggle(this.props.isToggle));
    }
}

export default QuestionFormAnswers;