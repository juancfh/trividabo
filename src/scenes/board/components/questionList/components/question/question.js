import React, { Component } from 'react';
import QuestionFormAnswers from './components/questionFormAnswers/questionFormAnswers';
import QuestionInfo from './components/questionInfo/questionInfo';
import QuestionTimmer from './components/questionTimmer/questionTimmer';
import "./question.scss";

const questionTime=30;

class Question extends Component {

    constructor(props) {
        super(props);
        this.handleAnswerQuestion = this.handleAnswerQuestion.bind(this);
    }

    handleAnswerQuestion(answer) {
        let newQuestion = {...this.props.question};
        newQuestion.answer = answer;
        newQuestion.isAnswered = true;
        newQuestion.isToggle = true;
        this.props.onAnswer(newQuestion);
    }

    isCorrectAnswer() {
        return this.props.question.answer === this.props.question.number ? true: false;
    }

    render() {
        let isCorrect = this.isCorrectAnswer();
        return (
            <div className="question">
                <QuestionTimmer isToggle={this.props.question.isToggle} time={questionTime} onTime={this.handleAnswerQuestion}></QuestionTimmer>
                <p className="question__description">{this.props.question.description}</p>
                <QuestionFormAnswers isToggle={this.props.question.isToggle} answers={this.props.question.answers} onAnswer={this.handleAnswerQuestion}></QuestionFormAnswers>
                <QuestionInfo isToggle={this.props.question.isToggle} isCorrect={isCorrect} answer={this.props.question.number}></QuestionInfo>
            </div>
        );
    }
}

export default Question;