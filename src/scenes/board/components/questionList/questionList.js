import React, { Component } from 'react';
import Question from './components/question/question';


class QuestionList extends Component {

    constructor(props) {
        super(props);
        this.handleAnswerQuestion = this.handleAnswerQuestion.bind(this);
    }

    handleAnswerQuestion(question){
        this.props.onAnswer(question);
    }

    newQuestion(question) {
        return <Question className="board__question" onAnswer={this.handleAnswerQuestion} question={question} key={question.number}/>;
    }

    questionListComponents(questions) {
        let listComponents = questions.map((question) => this.newQuestion(question));
        return listComponents;
    }

    render() {
        const listComponents = this.questionListComponents(this.props.questions);
        return (listComponents);
    }
}

export default QuestionList;