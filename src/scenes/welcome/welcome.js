import React, { Component } from 'react';
import "./welcome.scss"
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class TrividavoWelcome extends Component {
    render() {
        return (
            <div className="welcome">
                <h2 className="welcome__title-h2">TRIVIDABO</h2>
                <p className="welcome__text-simple">Welcome to trividabo number quiz!</p>
                <Link to="/board">
                    <button className="welcome__button">START</button>
                </Link>
            </div>
            );
        }
    }

export default TrividavoWelcome;