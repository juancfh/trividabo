import React, { Component } from 'react';
import ErrorBoundary from './components/errorBoundary';
import Board from './scenes/board/board';
import TrividavoWelcome from './scenes/welcome/welcome';
import './styles.scss';
import { BrowserRouter as Router, Route } from "react-router-dom";

class App extends Component {
    render() {
        return (
            <ErrorBoundary>
                <Router>
                    <div className="app">
                        <Route exact path="/" component={TrividavoWelcome} />
                        <Route path="/welcome" component={TrividavoWelcome} />
                        <Route path="/board" component={Board} />
                    </div>
                </Router>
            </ErrorBoundary>
            );
        }
    }

    export default App;
    