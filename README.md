### Trividabo

[Descripción del proyecto](https://github.com/ElParking/code-test/blob/master/test-front-end.md)

### Funcionamiento

La app ha sido creada con [react-create-app](https://facebook.github.io/create-react-app/).

Esta desplegada en [esta url](http://trividabo.herokuapp.com/). Por el momento solo esta disponible bajo el protocolo HTTP.

Para arrancar la aplicación en modo de desarrollo basta con ejecutar: 

```
npm install
npm run start
```

### Arbol de componentes

La imagen inferior muestra el arbol de componentes con el flujo de datos que hay entre ellos.

![tree](/docs/components-tree.jpg)

### Estructura de ficheros

Para la estructurar el proyecto he decido seguir la estructura propuesta en este [artículo](https://medium.com/@alexmngn/how-to-better-organize-your-react-applications-2fd3ea1920f1)

En mi caso solo tengo dos escenas: 

* board: La que representa el tablero de juego donde aparecen las preguntas. 
* welcome: La pantalla inicial.

### Test

Se han realizado casos de test en las tres capas: 

* unitarios
* integración
* end-to-end

Para ejecutar los test unitarios y de integración

```
npm run test
```

Para ejecutar los test end-to-end

```
npm run e2e
```

Los test unitarios y de integración se encuentran en la ruta `src/__tests__`. Mientras que los test e2e están en `e2e/`.

Los test unitarios y de integración se realizán utilizando [Jest](https://jestjs.io/). Los test e2e utilizando [puppeteer](https://pptr.dev/). 
La configuración de Jest viene definida por react-create-app(https://facebook.github.io/create-react-app/) y la de puppeteer puede consultarse en el fichero
`jest-puppeteer.config.js`.

### ToDos

- [ ] Manejar el estado de la aplicación con redux
- [ ] Icono de carga para las peticiones a la API
- [ ] Terminar test de integración y e2e
- [ ] Barra de progreso para el tiempo de las preguntas (cambia de color)
- [ ] Diseño totalmente responsive
- [ ] Persistir el estado de la aplicación para permitir la recarga de la página
- [ ] Systema de log
- [ ] Página 404 NOT FOUND
- [ ] Generar respuestas incorrectas en un rango similar al de las incorrectas
- [ ] Colocar las respuestas en posiciones aleatorias

### Autor

* Nombre: Juan Carlos Frutos Hernández 
* Email: juancarfruher@gmail.com